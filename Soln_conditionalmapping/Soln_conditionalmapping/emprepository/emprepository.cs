﻿using Soln_conditionalmapping.EF;
using Soln_conditionalmapping.emprepository.Interface;
using Soln_conditionalmapping.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_conditionalmapping.emprepository
{
    public class emprepository : Iemprepository
    {
        #region declaration
        private empdbentities db = null;
        #endregion

        #region constructor
        public emprepository ()
        {
            db = new empdbentities();
        }
        #endregion

        #region public method
           public async  Task<IEnumerable<empentity>> getempterminatedate()
        {
            try
            {
                return await Task.Run(() => {

                    var getquery =
                        db
                       .tblemps
                        .AsEnumerable()
                        .Select((letblempobj) => new empentity()
                        {
                            empid =(Int)letblempobj.empid,
                            empfirstname = letblempobj.empfirstname,
                            emplastname = letblempobj.emplastname,
                            isterminate = true
                        })
                        .ToList();

                    return getquery;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
