﻿using Soln_conditionalmapping.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_conditionalmapping.Entity
{
   public class empentity : Iempentity
    {
       public int empid { get; set; }
       public string empfirstname { get; set; }
       public string emplastname { get; set; }
       public Boolean isterminate { get; set; }
    }
}
