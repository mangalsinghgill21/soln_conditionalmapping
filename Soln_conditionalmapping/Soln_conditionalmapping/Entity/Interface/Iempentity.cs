﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_conditionalmapping.Entity.Interface
{
      public  interface Iempentity
    {
        public int empid { get; set; }
        public string empfirstname { get; set; }
        public string emplastname { get; set; }
        public Boolean isterminate { get; set; }
    
    }
}
